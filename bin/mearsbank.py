import user, permission

# main python package for mearsbank

class MearsBank:
    @staticmethod
    def getUserIDfromSlackID(cls, slackID):
        user.User(slackID).getUserID()

    @staticmethod
    def resignMint(cls, requestorID, recipientID):
        if permission.Permission.queryPermStatus(requestorID.getUserID(), "MINTCOIN"):
            permission.Permission(requestorID, "MINTCOIN", False)
            permission.Permission(requestorID, "SENDCOIN", True)
            permission.Permission(requestorID, "EDITPERM", True)

            permission.Permission(recipientID, "SENDCOIN", False)
            permission.Permission(recipientID, "BIDWCOIN", False)
            permission.Permission(recipientID, "EDITPERM", False)
            permission.Permission(recipientID, "TERMUSER", False)
            permission.Permission(recipientID, "MINTCOIN", True)
            return True
        return False

    @staticmethod
    def provisionAdmin(cls, requestorID, recipientID):
        if permission.Permission.queryPermStatus(recipientID, "MINTCOIN"):
            return False

        if permission.Permission.queryPermStatus(requestorID, "EDITPERM"):
            permission.Permission(recipientID, "EDITPERM", True)
            permission.Permission(recipientID, "SENDCOIN", True)
            permission.Permission(recipientID, "BIDWCOIN", False)
            permission.Permission(recipientID, "TERMUSER", False)
            return True
        return False

    @staticmethod
    def provisionUser(cls, requestorID, recipientID):
        if permission.Permission.queryPermStatus(recipientID, "MINTCOIN"):
            return False

        if permission.Permission.queryPermStatus(requestorID, "EDITPERM"):
            permission.Permission(recipientID, "EDITPERM", False)
            permission.Permission(recipientID, "SENDCOIN", False)
            permission.Permission(recipientID, "BIDWCOIN", True)
            permission.Permission(recipientID, "TERMUSER", False)
            return True
        return False

    @staticmethod
    def sendGift(cls, requestorID, UserID, amount):
        return user.User.getUserFromUserID(requestorID).gift(amount, user.User.getUserFromUserID(UserID))

    @staticmethod
    def purchase(cls, UserID, itemName, itemPrice):
        return user.User.getUserFromUserID(UserID).purchase(itemName, itemPrice)

    @staticmethod
    def getBalance(cls, UserID):
        return user.User.getUserFromUserID(UserID).getBalance()

    @staticmethod
    def getMarketSize(cls):
        activeUsers = user.User.getAllActiveUsers()
        total = 0
        for eachUser in activeUsers:
            total += eachUser.getBalance()

        return total
