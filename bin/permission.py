import dbmanager, datetime, permcode


# refers to a Permission object in our database, associated with an update of perm codes

class Permission:
    def __init__(self, UserID, PermName, Enabled):
        Timestamp = datetime.datetime.now()
        PermCode = permcode.PermCode(PermName).getPermCode()

        dbm = dbmanager.DBManager("MearsBank")
        dbm.runCommand("INSERT INTO Permissions (UserID, PermCode, Timestamp, Enabled) VALUES (%d, %d, '%s', %s)" % UserID, PermCode, Timestamp, Enabled)

    @staticmethod
    def queryPermStatus(cls, UserID, PermName):
        pc = permcode.PermCode(PermName)
        dbm = dbmanager.DBManager("MearsBank")
        result = dbm.runCommand("SELECT Enabled FROM Permissions WHERE UserID = %d && PermCode = &d ORDER BY Timestamp DESC LIMIT 1" % UserID, pc.getPermCode())[0][0]
        return result