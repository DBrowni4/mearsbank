import dbmanager, transaction, permission, slackintegrator

# represents a user in our system

class User:
    @staticmethod
    def userCount(cls):
        dbm = dbmanager.DBManager("MearsBank")
        return dbm.runCommand("SELECT COUNT(UserID) FROM Users")[0][0]

    @staticmethod
    def getAllUserIDs(cls):
        dbm = dbmanager.DBManager("MearsBank")
        out = []
        data = dbm.runCommand("SELECT UserID FROM Users")
        for eachLine in data:
            out.append(eachLine[0])

        return out

    @staticmethod
    def getAllUsers(cls):
        out = []
        userIDList = User.getAllUserIDs()
        for eachID in userIDList:
            out.append(User.getUserFromUserID(eachID))

        return out

    @staticmethod
    def getAllActiveUsers(cls):
        userList = User.getAllUsers()
        out = []
        for eachUser in userList:
            if not permission.Permission.queryPermStatus(eachUser.__UserID, "TERMUSER"):
                out.append(eachUser)

        return out

    @staticmethod
    def getUserFromUserID(cls, UserID):
        dbm = dbmanager.DBManager("MearsBank")
        slackID = dbm.runCommand("SELECT SlackID FROM Users WHERE UserID = %d" % UserID)[0][0]
        return User(slackID)

    def __init__(self, slackID):
        dbm = dbmanager.DBManager("MearsBank")
        result = dbm.runCommand("SELECT COUNT(UserID) FROM Users WHERE SlackID == %s" % slackID)
        if result[0][0] == 0:
            dbm.runCommand("INSERT INTO Users (SlackID) VALUES (%s)" % slackID)

        result = dbm.runCommand("SELECT * FROM Users WHERE STRCMP(SlackID, %s) = 0" % slackID)[0]
        self.__UserID = result[0]
        self.__SlackID = result[1]

        if User.userCount() == 1:
            permission.Permission(self.__UserID, "MINTCOIN", True)
            permission.Permission(self.__UserID, "EDITPERM", True)
        else:
            permission.Permission(self.__UserID, "BIDWCOIN", True)

    def getBalance(self):
        transactions = transaction.Transaction.getTransactionsByUserID(self.__UserID)
        if len(transactions) == 0:
            return 0

        total = 0

        for eachTransaction in transactions:
            total += eachTransaction.getTransactionAmount()

        return total

    def purchase(self, itemName, itemCost):
        if self.getBalance() < itemCost or not permission.Permission.queryPermStatus(self.__UserID, "BIDWCOIN"):
            return False

        transaction.Transaction.addTransaction(self.__UserID, -1 * itemCost, "Purchase of %s for $%d." % itemName, itemCost)
        return True

    def gift(self, amount, otherUser):
        if permission.Permission.queryPermStatus(self.__UserID, "MINTCOIN"):
            transaction.Transaction.addTransaction(otherUser.__UserID, amount, "Gift from %s to %s." % self.getName(), otherUser.getName())
        elif permission.Permission.queryPermStatus(self.__UserID, "SENDCOIN"):
            if self.getBalance() < amount:
                return False
            transaction.Transaction.addTransaction(otherUser.__UserID, amount, "Gift from %s to %s." % self.getName(), otherUser.getName())
            transaction.Transaction.addTransaction(self.__UserID, -1 * amount, "Gift from %s to %s." % self.getName(), otherUser.getName())
        else:
            return False

        return True

    def getName(self):
        integrator = slackintegrator.SlackIntegrator(self.__SlackID)
        return integrator.getRealName()

    def getUserID(self):
        return self.__UserID