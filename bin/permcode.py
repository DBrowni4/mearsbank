import dbmanager

# refers to a PermCode object in our database

class PermCode:
    @staticmethod
    def getAllCodes(cls):
        dbm = dbmanager.DBManager("MearsBank")
        out = []
        data = dbm.runCommand("SELECT PermCode FROM PermCodes")
        for eachLine in data:
            out.append(eachLine[0])

        return out

    @staticmethod
    def getAllCodeNames(cls):
        dbm = dbmanager.DBManager("MearsBank")
        out = []
        data = dbm.runCommand("SELECT PermName FROM PermCodes")
        for eachLine in data:
            out.append(eachLine[0])

        return out

    def __init__(self, PermName):
        dbm = dbmanager.DBManager("MearsBank")
        self.__PermCode = dbm.runCommand("SELECT PermCode FROM PermCodes WHERE STRCMP(PermName, %s) = 0" % PermName)
        self.__PermName = PermName

    def getPermCode(self):
        return self.__PermCode

    def getPermName(self):
        return self.__PermName
