import datetime, dbmanager

# represents a transaction in our system. any action that changes the balance of a user is a transaction

class Transaction:
    @staticmethod
    def addTransaction(cls, UserID, TransactionAmount, TransactionComment):
        dbm = dbmanager.DBManager("MearsBank")
        TransactionTime = datetime.datetime.now()
        dbm.runCommand("INSERT INTO Transactions (UserID, TransactionAmount, TransactionTime, TransactionComment) VALUES (%d, %d, %s, %s", (UserID, TransactionAmount, TransactionTime, TransactionComment))

    @staticmethod
    def getTransactionByTransactionID(cls, TransactionID):
        dbm = dbmanager.DBManager("MearsBank")
        data = dbm.runCommand("SELECT * FROM Transactions WHERE TransactionID == %d" % TransactionID)[0]

        newTransaction = Transaction()
        newTransaction.__TransactionID = data[0]
        newTransaction.__UserID = data[1]
        newTransaction.__TransactionAmount = data[2]
        newTransaction.__TransactionTime = data[3]
        newTransaction.__TransactionComment = data[4]

    @staticmethod
    def getTransactionsByUserID(cls, UserID):
        dbm = dbmanager.DBManager("MearsBank")
        data = dbm.runCommand("SELECT TransactionID FROM Transactions WHERE UserID == %d" % UserID)

        transactions = []

        for eachTransaction in data:
            transactions.append(Transaction.getTransactionByTransactionID(eachTransaction[0]))

        return transactions


    def getTransactionID(self):
        return self.__TransactionID

    def getUserID(self):
        return self.__UserID

    def getTransactionAmount(self):
        return self.__TransactionAmount

    def getTransactionTime(self):
        return self.__TransactionTime

    def getTransactionComment(self):
        return self.__TransactionComment