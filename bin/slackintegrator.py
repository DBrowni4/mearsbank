from slack_sdk import WebClient
import yaml

# class to help integrate slack api

class SlackIntegrator:
    def __init__(self, SlackID, configFile="/config/slackconf.yaml"):
        self.__SlackID = SlackID
        self.__loadConfig(configFile)


    def __loadConfig(self, configFile):
        with open(configFile, 'r') as config:
            fields = yaml.safe_load(config)
            self.__token = fields["token"]

    def getRealName(self):
        client = WebClient(self.__token)
        return client.users_info(self.__SlackID)["user"]["profile"]["real_name_normalized"]

    def startAuction(self, channel, itemName, startingPrice):
        client = WebClient(self.__token)
        client.chat_postMessage(channel, blocks=[
            {
                {
                    "type": "section",
                    "text": {
                        "type": "plain_text",
                        "text": "Auction Alert: %s Starting at %d" % (itemName, startingPrice)
                    }
                },
                {
                    "accessory": {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "text": "Bid now!"
                        },


                    }
                }
            }
        ])