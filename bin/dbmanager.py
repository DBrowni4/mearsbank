import mysql.connector, yaml


# manages our connections to the database

class DBManager:
    def __init__(self, database, configFile="config/dbconf.yaml"):
        with open(configFile, "r") as config:
            values = yaml.safe_load(config)
            host = values["host"]
            try:
                port = int(values["port"])
            except:
                return Exception()

            username = values["user"]
            password = values["password"]

            self.__db = mysql.connector.connect(host=self.__host, port=port, username=username, password=password, database=database)
            return

        return Exception()

    def runCommand(self, command):
        runner = self.__db.cursor()
        runner.execute(command)
        runner.commit()
