CREATE DATABASE MearsBank;
USE MearsBank;

CREATE TABLE PermCodes (
    PermCode int NOT NULL AUTO_INCREMENT,
    PermName varchar(8) NOT NULL,
    PRIMARY KEY (PermCode)
    );

CREATE TABLE Users (
    UserID int NOT NULL AUTO_INCREMENT,
    SlackID varchar(9) NOT NULL,
    PRIMARY KEY (UserID)
    );

CREATE TABLE Permissions (
    PermissionID int NOT NULL AUTO_INCREMENT,
    UserID int NOT NULL,
    PermCode int NOT NULL,
    Timestamp DATETIME NOT NULL,
    Enabled bool NOT NULL,
    PRIMARY KEY (PermissionID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID),
    FOREIGN KEY (PermCode) REFERENCES PermCodes(PermCode)
    );

CREATE TABLE Transactions (
    TransactionID int NOT NULL AUTO_INCREMENT,
    UserID int NOT NULL,
    TransactionAmount int NOT NULL,
    TransactionTime DATETIME NOT NULL,
    TransactionComment varchar(128) NOT NULL,
    PRIMARY KEY (TransactionID),
    FOREIGN KEY (UserID) REFERENCES Users(UserID)
    );